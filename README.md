## Лабораторная работа 4. Создание, редактирование и удаление данных по сотрудникам

**Цель работы:** _Манипулирование с данными классов предметной области_

**В результате:** 

1. Cоздал окно WindowsNewEmployee _(рисунок 1)_
2. Добавил новые методы в класс Person _(рисунок 2)_ и PersonDPO _(рисунок 3)_
3. Создал кнопки для окна WindowEmployee _(рисунок 4):
	* Добавить
	* Редактировать
	* Удалить_
4. Протестировал _(рисунки 5-7)_


![WindowsNewEmployee_4](screenshots/WindowsNewEmployee_4.jpg)

_Рисунок 1 - Окно WindowsNewEmployee_



![Person_4](screenshots/Person_4.jpg)

_Рисунок 2 - Новый метод класса Person_



![PersonDPO_4](screenshots/PersonDPO_4.jpg)

_Рисунок 3 - Новые методы класса PersonDPO_



![btn_4](screenshots/btn_4.jpg)

_Рисунок 4 - Кнопки для работы со списком сотрудников_



![add_4](screenshots/add_4.jpg)

_Рисунок 5 - Дабавление сотрудника_



![edit_4](screenshots/edit_4.jpg)

_Рисунок 6 - Редактирование сотрудника_


![delete_4](screenshots/delete_4.jpg)

_Рисунок 7 - Удаление сотрудника_
