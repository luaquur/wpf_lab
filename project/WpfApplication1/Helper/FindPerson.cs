﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Model;
namespace WpfApplication1.Helper
{
    class FindPerson
    {
            int id;
            public FindPerson(int id)
            {
                this.id = id;
            }
            public bool PersonPredicate(Person role)
            {
                return role.Id == id;
            }
    }
}
